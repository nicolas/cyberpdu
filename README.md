## Cyper Power PDU remote control

This is a command wrapper around net-snmp-utils, you need to install
these command line utility first. To use this software:

./cyberpdu <pdu-ip> <socket-name> (on|off|reset)

The name are configured form the Web UI and the reset delay is also 
configured there.

## TODO

- Allow socket number to be used
